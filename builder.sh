cd /datadrive/presentations.voidspace.xyz
git pull
if git checkout develop &&
    git fetch origin develop &&
    [ `git rev-list HEAD...origin/develop --count` != 0 ] &&
    git merge origin/develop
then
    echo 'Updates available'
    docker build . -t voidspacexyz/pelican-builder
    docker run --rm -it -v $(pwd):/build/ voidspacexyz/pelican-builder:latest pelican -s publishconf.py -o live
else
    echo 'Nothing to update.'
fi
