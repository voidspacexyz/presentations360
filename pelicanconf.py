#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'VoidSpaceXYZ'
SITENAME = u'Presentations360'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
MENUITEMS = (('Home', 'https://voidspace.xyz/'),
         ('Blog', 'https://blog.voidspace.xyz'),
         ('Presentations', 'https://presentations.voidspace.xyz'),
         ('GitLab', 'https://gitlab.com/voidspacexyz'))
# Social widget
SOCIAL = (
          ('Twitter', 'https://twitter.com/voidspacexyz'),
          ('Facebook', 'https://facebook.com/voidspacexyz'),
          ('Diaspora', 'https://diasp.in/i/95a3c3c43c8d'),
          ('Email', 'mailto:null@voidspace.xyz'),
          )

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'theme'

# Site Definations
SITECSS = "purple lighten-1"
PHONENUMBER = 'null@voidspace.xyz'
#Mobile = '9999999'
