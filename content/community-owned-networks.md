---
Title: Community Owned Networks
Date: 2017-03-30 10:20
Modified: 2017-03-30 19:30
Category: Networks
Tags: mesh-network, community-control, data-commodification, self-hosting
Slug: community-owned-networks
Authors: Ramaseshan
Summary:  The Internet is widely a larger tool for human control. How do we break it off ? What are some of the first steps
Template: revealjs
Theme: simple
Intro: name
Contact: contact
Transition: concave
Draft: yes
---

    ## What is the "WEB" ? 

    (Psst ! Not the Spider web)
    
    ====

    Evolution of the Web today !

    ----

    ### A Local Network 

    * Like DARPA where it started

    ----

    ### Inter-Network

    ----

    ### The Internet

    * DOTCOM Bubble
    * Personal Websites
    * Cloud Servers
    * Hosted Services

    ====

    The biggest problem's of today's internet

	* Platform centralization
	* Data commodification
    * Control of Information
 
    ====

    ## <strike>De</strike> Centralized and Under Surveillance

    Platform Centralization

    ====


    
    (Fake) Digital Equality ?
        
    ====
    
    Mesh Networks or Community Owned Networks

    Old wine in New bottle

    ====

    What Next ?
