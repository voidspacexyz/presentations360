---
Title: Panopticon  - A System of Control
Date: 2018-02-10 06:10
Modified: 2018-02-10 06:10
Category: Networks
Tags: mesh-network, community-control, data-commodification, self-hosting
Slug: panopticon-a-system-of-control
Authors: VoidSpaceXYZ
Summary:  The Internet is widely a larger tool for human control. How do we break it off ? What are some of the first steps
Template: revealjs
Theme: simple
Intro: name
Contact: contact
Transition: concave
Draft: yes
---

    ### What is Technology

    Note:
    - Run through what a computer or a mobile phone is
    - Question if is this all technology is all about ?
    - Some alternative models of technology
    - Estimate 5 min

    ====

    ### What is Data

    Note:
    - Keep it detached from internet, and run through other examples of data
    - Weather Data or Govt Employee attendance Data (http://attendance.gov.in/)

    ====

    ### What is Privacy

    ----

    #### noun: a state in which one is not observed or disturbed by other people.

    Note:
    - Generally talk about CCTV or personal intimate moments etc
    - Go through to explain who you are and what you are differences
    - Identity and the need for it
    - Identity and the no need for it
    - Where does privacy end ?

    ----

    #### Categories of Digital Data

    - Common Public Data
    - Personal Public Data
    - Private Data
    - Secret Data

    Note:
    - Name as personal public data, weather data as common public data, Income Tax as private data, and the bank account passwords as secret data

    ----

    ### Privacy and Data

    Note:
    - The need to protect private and secret data

    ====

    ### Fundamental Right to Privacy

    - Article 14 (Right to Equality)
    - Article 19 (Freedom of Speech)
    - Article 21 (Right to Life and Personal Liberty)

    Note:
    - life is more than mere animal existence
    - No person shall be deprived of his life or personal liberty except according to procedure established by law.
    - Right To Live with Human Dignity
    - Right Against Sexual Harassment and Rape
    - Right to Reputation
    - Right To Livelihood and Shelter
    - Right to Social Security and Protection of Family
    - Right to Health and Medical Care
    - Right to get Pollution Free Water and Air, Clean Environment
    - Right to Know or Right to Be Informed
    - Right against Illegal Detention
    - Personal Liberty -> Right to Privacy
    - Personal Liberty -> Woman’s Right to Make Reproductive Choices

    ====

    ### Data Commodification

    - What is commodification ?
    - What can you gain by selling data ?

    ----

    - What is the buisness model of Facebook or Google ?
    - How is Facebook valued at 300 Billion Dollars ?
    - A look back at Free Basics or Internet.org
    - Free Basics was not about connecting people, it was about making a better buisness around the un-connected.

    ----

    - Ownership of Data
    - Ownership of Services
    - Profits and Control through data
    - A new model of business transaction

    ====

    ### Data Commodification by Governments

    - NSA
        - Upstream Collection
        - DataCenters or SaaS and Data Collection
    - Aadhaar
        - Authentication Models and Right to Privacy
        - Enterprise Aadhaar
        - https://www.trustid.in/ or Mobile Operators or Bank Accounts
        - Pricing Model

    Note:
    - NSA - https://thedaywefightback.org/
    - Trash Aadhaar
    - aadhaar.fail

    ----

    ### A 360 degree view of the Citizen

    ====

    ### 3A's of Technology

    - Availability
    - Accessibility
    - Affordability

    ====

        ### Centralization and Decentralization

        - Decentralize, Federate and Co-Operate
        - Riot as an Example of Decentralization

    ====

        Free Software Movements and their Role

    ====

    ### How to Protect yourself

    ====

    ### How to contribute 
