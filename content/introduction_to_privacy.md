---
Title: Privacy Matters 
Date: 2020-04-08
Modified: 2020-04-08
Category: Privacy
Tags: privacy, rights
Slug: privacy-matters 
Authors: VoidSpaceXYZ
Summary: Privacy has many facets. This presentation is an attempt to help the commons understand the aspect of privacy.
Template: revealjs
Theme: simple
Intro: name
Contact: contact
Transition: concave
---
    ### “as technology improves, privacy as we know it will inevitably evaporate; the best we can hope for is the power to watch the watchers."  
    -- "1984" by George Orwell


    ====

    ## History of privacy in the Indian context

    ----

    ![No_rights_to_privacy](theme/img/privacy/no_right_to_privacy.png)

    Note:
    - https://timesofindia.indiatimes.com/india/No-fundamental-right-to-privacy-to-citizens-Centre-tells-SC/articleshow/48171323.cms

    ----

    ![No_rights_over_body](theme/img/privacy/no_absolute_right_over_bodies.png)

    Note: 
    - https://twitter.com/SFLCin/status/859326576693063680
    - https://thelogicalindian.com/news/supreme-court-aadhaar-card
    - https://timesofindia.indiatimes.com/india/citizens-dont-have-absolute-right-over-their-bodies-government/articleshow/58486260.cms


    ----

    ### Fundamental Right to Privacy under Article 21 and Part 3 of the constitution

    Note:
    - https://main.sci.gov.in/supremecourt/2012/35071/35071_2012_Judgement_24-Aug-2017.pdf


    ---- 

    ![Poor have the right to question and have all the rights](theme/img/privacy/poor_have_the_right_to_question.png)
  
    ====

    ## What is privacy

    ----

    - Decisional autonomy
    - Spatial control
    - Informational privacy


    ====

    ## Decisional autonomy

    ----

    ![SC_Judgement_Individual choice and autonomy](theme/img/privacy/individual_choice_and_autonomy.png)

    ----

    - Limiting external (state and non-state) intrusions from interfering with  one’s decision making.
      - Eg: Government forcing an individual to say something or write down something.
  
    ----

    ## Bodily integrity 

    ---- 

    - Right to Dignity
    - Right to reproductive choices(abortion etc)
    - Right to sexual choices(partners, methods etc) 

    Note: 
    - https://invidio.us/watch?v=aMxyX2OzKPk (Human Rights and Bodily Integrity - 2014)
    - https://invidio.us/watch?v=6e7IpvddZpg ( Bodily autonomy: my body, my choice? - 2017)

    ----

    - "The Transgender Persons (Protection of Rights) Bill, 2019" and its violation of individual choice an autonomy.

    Note:
    - https://www.thenewsminute.com/article/equal-killing-us-why-indias-transgender-community-rejecting-trans-bill-93579
    - https://www.prsindia.org/billtrack/transgender-persons-protection-rights-bill-2019 (Specifically read Certificate of identity for a transgender person section)

    ====

    ## Spatial control 

    ----

    - Privacy at one's home
    - Spatial control is broadly understood as one’s control over their private spaces for personal relations and family life

    ====

    ## Informational Privacy

    ----

    - Retaining personal control over personal information 
    - Personal information includes 
      - Information we store online
      - information that we generate by using various applications and online services 
    
    ----

    - In the lines of "spatial control"  
      - Usage of tools like whatsapp, skype or tinder or cab aggregators. 
      - Spatial control extends to include control over access to location, movement and communication. 

    ----

    - In the lines of "decisional autonomy"  
      - Targetted ads, trying to influence our shopping habits, political opinions etc
      - Recommendation algorithms, targeted ads, personalization of news feed – all try to chip away users’ decisional autonomy, one bit at a time. 

    ----

    The dangers posed before Informational privacy

    ----

    1. Permanence : The internet never forgets (Digital footprints)
    2. Invisibility : An individual’s digital information can be accessed without their knowledge.
    3. Recombinant: Individual distinct peices of information can be put together to reveal deep personal tendencies of an individual. 

    ----

    1. CCTV survillence systems and mass survillence 
    2. Social media monitoring hub and the bill

    Note:
    - Social media monitoring hub in the year 2018 (TODO: Bill link to be updated)
    

    ==== 

    ![The privacy diagram](theme/img/privacy/the_privacy_diagram.png)

    ----

    Emphasising on 2 aspects of freedom : 


    1. The freedom to be let alone
    2. The freedom to self development

    ----

    A small poem from the book "The Assault on Privacy" by Felicia Lamport


    ```
    DEPRIVACY
        Although we feel unknown, ignored
        As unrecorded blanks,
        Take heart! Our vital selves are stored
        In giant data banks,
        Our childhoods and maturities,
        Efficiently compiled,
        Our Stocks and insecurities,
        All permanently filed,
        Our tastes and our proclivities,
        In gross and in particular,
        Our incomes, our activities
        Both extra-and curricular.
        And such will be our happy state
        Until the day we die
        When we’ll be snatched up by the great
        Computer in the Sky
    ```

    ====

    ![SC To live is to live with dignity](theme/img/privacy/to_live_is_to_live_with_dignity.png)

    ====

    ## Stand up and speak up, 
    ## for not its my right that is in question, its yours too.

    ---- 

    ## Privacy matters !