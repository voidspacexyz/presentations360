---
Title: Free Software Movements
Date: 2018-02-10 06:10
Modified: 2018-02-10 06:10
Category: Free Software
Tags: free-software,privacy, free-software-movements, introduction
Slug: intro-to-free-software-movements
Authors: VoidSpaceXYZ
Summary:  Free Software has the ability to bring a large change ? A change that can enable people. But what is free software ? What is a free software movement ? How is it relevant. Lets explore more.
Template: revealjs
Theme: beige
Intro: name
Contact: contact
Transition: convex
Draft: yes
---

    ### What is Technology

    Note:
    - Run through what a computer or a mobile phone is
    - Question if is this all technology is all about ?
    - Some alternative models of technology
    - Estimate 5 min

    ====

    ### Who are people ?

    Note:
    - Can we just say the people inside this auditorium are people  ?
    - What about the anganwadi workers ?
    - What about the beggers ?
    - Vijay mallaya ? Nirav Modi ?

    ====

    ### People + Technology 

    ----

    How does Technology enable people ?

    ----

    Technology will assist or replace ?

    ====

    #### Free Software as a model for growth 

    ----

    What is Free Software ?

    ----

    Contributors, Contributions and Licenses 

    ---- 

    Government and Free Software 

    Note:
    - Kerala Government has completely moved to free software 
    - GOI in 2017 , has made it mandatory to consider OSS in all RFP. Implementation is still pending and a long way. But the push has been for mandatory use of FOSS in all public institutions.

    ====

    ### The Need for Free Software 

    ----

    ### Economic prespective 

    Note: 
    - How Freedom reduces the cost of software
    - The values of communities coming together 
    - Community Center and what has it enabled.

    ----

    ### Skill Prespective 
   
    * Git along with GitLab/Github 
    * Student Developers 

    Note: 
    - Talk a little about distributed development models 
    - Decentralisation 
    - Gitlab , FOSS and their openness 
    - github having the largest repository of code 
    - Show some projects and run through some of them 

    ----

    ### Why does it all matter ?

    Note:
    - Explain differences between prop and FOSS 
    - The economic prespective 
    - Adobe sued all local xerox shop in andra and a large fight happened 
    - How prop enables control 
    - convinence vs freedom 
    - espically in govt institutions why FOSS 
    - Anti-Virus as an example . (clamav as an alternative)
    - List of FreeSoftware I use (https://gitlab.com/snippets/1697735) 

    ----

    ### Why do people contribute 

    Note:
    - People need to be given the first value 
    - Personal Growth 
    - The value of community 
    - Industry Needs are growing at a phase 
    - Thus also proves the point that, people growth can only happen together and not as corporates 

    ----

    ### Open Source . Say NO.
    ### Free Software . Say YES.

    Note:
    - Explict stress on why looking at code alone is a very bad idea 
    - Why such thoughts needs to be avoided 
    - Also differences between FOSS and Open source 

    ====

    ### Skill Inclusivity 

    What about people who cant/dont like to code ?

    ---- 

    Localisation 

    Note:
    - NammaFedora 
    - Language inclusivity 
    - Adoption 

    ----

    OpenStreepMap 

    Note:
    - Map Data  (www.OpenStreepMap.org)
    - Contributions
    - ShowCase: https://github.com/geohacker/bmtc
    - Overpass turbo (http://overpass-turbo.eu/)
    - OSMAnd and Maps.me 

    ----

    Freedom Hardware 

    Note:
    - FarmBot (https://farmbot.io/)
    - Community Owned weather Stations Pondicherry (https://discuss.fsftn.org/t/building-a-low-cost-community-weather-station/360/11)
    - SmartCitizens(https://smartcitizen.me/)
    - Open Motors (https://openmotors.co/download/)

    ---- 

    Free Knowledge Portals

    Note: 
    - Wikipedia 
    - Scihub 
    - Stress on the point of Knowledge Freedom and why it has to be in the commons 

    ----
    
    Evangalism 

    ----

    More . You are welcome to invent your own roles too.

    ====

    ### Internet and Free Software 

    ----

    ### Data as a buisness model 

    Note:
    - How companies sell your data to make profits 
    - When did you shift from a customer to an object of profit 
    - Facebook or other buisness models. Mention about Free Basics too here 
    
    ----

    ### Privacy By Design 

    Note: 
    - What is Privacy  and why is it important 
    - Looking at privacy from the digital world 
    - Go ahead, trash aadhaar for 5 min and nothing more 
    - Europe as an example. If companies compromise privacy , then the companies will have to compensate 10% of the total profit to the customer 
    - US has a Data leak reporting policy 

    ----

    ### Free Software enables Privacy

    Note:
    - Auditable code 
    - Trust and self running ability 

    ====

    ### Buisness Models and Free Software 
    
    * Contribution/ Donations 
    * Services (RedHat)
    * Selling Software itself 
    * Software as a service 
    * Companies pay to build FOSS  (IBM and Linux Kernel)

    ====

    ### Open Source Vs Freedom Software

    ====

    ### Women and Technology 

    * Stereotypes
    * Restrictions 
    * Patriarchy

    ----

    ### Gender Inclusivity and its need 

    ----

    ### Models to enable gender discriminated people 

    * OutReachy 
    * Django/Ruby Girls, CodeLikeAGirl
    * FSMK Women's Wing 
    * Gender inclusive institutions 

    ----

    ### Why is it important to stand up

    ----

    ### Who can contribute to the growth 

    * Discrimination needs to end. 
    * Freedom is important and cant be compromised

    ====

    ### FSMK and its role

    ----

    * FSMK camps 
    * GLUGS and its important and role 
    * Community Center 
    * Women's Wing (Embedded For Her)
    
    ----

    ### Why should I join ?

    * If all this made sense to you, I think you should consider 
    * Collective Growth 
    * Freedom Knowledge (whereever we can)

