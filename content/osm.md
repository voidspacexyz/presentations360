---
Title: Introduction to OpenStreetMap(OSM)
Date: 2020-04-25 12:00
Category: OpenData
Tags: maps, osm, opendata,
Slug: intro-to-osm
Authors: Ramaseshan
Summary:  Contributing to open map data
Template: revealjs
Theme: simple
Intro: name
Contact: contact
Transition: concave
Draft: true
---

    ## Open Data

    ----

    - Publically licensed 
    - Community accessible (mostly also contributed)
    - Usage and analysis free of copyright norms 

    ====

    ## Open Maps and Map Data

    ----

    - Maps are informative
    - Maps are visual symbols of the world
    - Helps plan resource utilisation, availability

    ----

    # Information is powerful

    ====

    # OpenStreetMap

    ----

    ### Activitiy 

    Lets take a pencil and paper, and draw our street, with everything we think is essential. 

    ----

    - Open Map data
    - [Publically licensed](https://www.openstreetmap.org/copyright)
    - User contributable (like Wikipedia)
    - WebMap
    - We understand and know our street much better than any company. 
    - Access to historical changes to a point, street etc

    ----

    ## Open Licensed GeoData

    ----

    <iframe src="https://publicgeodata.org/arguments/" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
    

    ----

    [Lets look at a sample mapped data](https://www.openstreetmap.org/node/652721136#map=15/12.5170/76.8981)

    ====

    ### Mapping in OSM

    ----

    ### Browser / Desktop / Laptops

    ----

    [Open OSM Website](https://openstreetmap.org)

    ----

    ### Mobile 

    ----

    [OSMAnd~](https://f-droid.org/app/net.osmand.plus)  
    [OSMTracker](https://f-droid.org/app/net.osmtracker)

    ----

    ### Demo

    ====

    ### Usecases

    ----

    #### List of all aminities in a place 

    - Schools 
    - Drinking water 
    - highway

    ----

    <iframe src="https://overpass-turbo.eu/s/Thg" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">

    Note:
    - https://wiki.openstreetmap.org/wiki/Key:amenity


    ---- 

    [Open Map BMTC Data](https://github.com/geohacker/bmtc)

    ----

    <iframe src="http://opencity.in/content_type/map" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">

    ----

    ### [OSM Diaries](https://www.openstreetmap.org/diary)

    ====

    How not to map is OSM

    ---- 

    - Data from copyrighted maps 
    - Overlaying from copyrighted maps 
    - Re-mapping from copyrighted maps
    - Mapping a place you have not verified or seen

    ====

    ### Why not Google Maps

    ----

    - No access to underlying data (but its data about us and around us)
    - Dont allow deriving data from their maps
    - Dont allow copying & re-dustributing with a different license
    - High costs for usage of data
    - No cycle routes or footpaths etc
    - Google branding everywhere
      - Comes along google tracers tracking you.

    ==== 

    ### Sustaining your contributions

    ----

    - Map
    - Explore JOSM and other tools 
    - Cartography
    - Forest, tree mapping
    - More, your imagination is your limit. There is defination for everything.

    ----

    <iframe src="https://learnosm.org/en/beginner/introduction/" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">


    ----

    #### Reachout to the workers of [Chiguru.tech](https://chiguru.tech) if you want to organize a Mapping Party in your location.


