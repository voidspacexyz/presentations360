---
Title: Understanding Survillence
Date: 2020-04-10 06:10
Modified: 2020-04-10 06:10
Category: Networks
Tags: mesh-network, community-control, data-commodification, self-hosting
Slug: survillence-capitalism-slavery
Authors: VoidSpaceXYZ
Summary:  The Internet is widely a larger tool for human control. How do we break it off ? What are some of the first steps
Template: revealjs
Theme: simple
Intro: name
Contact: contact
Transition: concave
Draft: no
---
    ## "If you’ve got nothing to hide, you are nothing."

    ----


    ## “it is no longer enough to automate information flows about us; the goal now is to automate us.” 

    ====

    ![Survillence_Tools](theme/img/survillence/technological_tools.png)  <!-- .element: style="width: 80%" -->

    ====

    ![Google](theme/img/privacy/google.png)
    ![youtube](theme/img/survillence/youtube.com.png)


    ----

    How much does Google (now Alphabet) make out of targetted advertisments ?

    ----

    https://myactivity.google.com
    
    ----

    - **~94%** revenue from ads
    - Performance advertising, Brand advertising and the others. 

    ----

    <iframe src="https://abc.xyz/investor/static/pdf/2019Q4_alphabet_earnings_release.pdf?cache=05bd9fe" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
    
    Note:
    - https://abc.xyz/investor/static/pdf/2019Q4_alphabet_earnings_release.pdf?cache=05bd9fe
    - https://dashboards.trefis.com/no-login-required/HMtQjcWW/Google-Revenues-How-Does-Google-Make-Money-


    ====

    ## Facebook family of productions (Instagram, WhatsApp, facebook,com etc)

    ----

    <iframe src="https://s21.q4cdn.com/399680738/files/doc_financials/2019/q4/Q4-2019-Earnings-Presentation-_final.pdf" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">

    </iframe>

    Note:
    - https://s21.q4cdn.com/399680738/files/doc_financials/2019/q4/Q4-2019-Earnings-Presentation-_final.pdf


    ----

    Facebook and Suicide prevention AI algorithm

    Note:
    - https://www.businessinsider.in/tech/inside-facebooks-suicide-algorithm-heres-how-the-company-uses-artificial-intelligence-to-predict-your-mental-state-from-your-posts/articleshow/67409957.cms
    - https://techcrunch.com/2017/11/27/facebook-ai-suicide-prevention/

    
    ==== 

    ## OLA / Uber / Swiggy

    ### The Gig economy and predatory pricing. 

    ----

    ## Deriving insights from data is at the heart of everything we do at Ola  -- Sanjay Kharb, VP- Engineering, Ola

    Note:
    - https://cio.economictimes.indiatimes.com/news/business-analytics/inside-the-data-driven-model-of-ola-with-sanjay-kharb-vp-engineering-ola/71174439
    - 

    ====

    ## Amazon

    ----

    ### Consumer market capitalisation (Books, Movies, shopping etc)

    ====

    ##PayTM 

    ----
    
    - DataCentre in India along with AliBaba
    - Close to 80% stake by Chinese+American corporations

    ====

    ## Reliance 

    ----

    - Mobile Networks (has 5G permission, when BSNL is struggling to get 4G rights)
    - Aviation Industry
    - Coal mining
    - DataCenter (Localisation of Data)
    - Eventually will control the complete flow of all data and communication medium.

    ====

    ![GAFAM Octopus](theme/img/privacy/survillence/GAFAM-Octopus.png) <!-- .element: style="width: 120%" -->

    ====

    ![Top corporates list](theme/img/privacy/survillence/corporates_top_list.png)

    ====

    ![India dosent control its internet](theme/img/privacy/survillence/india_internet_no_control.png)

    ====

    NSA - We kill people on metadata  

    <a href="https://www.youtube.com/watch?v=UdQiz0Vavmc">Youtube : NSA Ex-Director Speech</a>

    ====

    ## Aadhaar 

    Note:
    - https://medium.com/karana/a-critical-examination-of-the-state-of-aadhaar-2018-report-by-idinsight-ef751e24d6c5
    
    ----

    ![Aadhaar for residents not citizens alone](theme/img/privacy/survillence/aadhaar_residents_id.png)

    ----

    <iframe src="https://lawescort.in/2019/09/linking-of-social-media-with-aadhaar-sc-asks-govt-to-share-plans/" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">

    </iframe>

    ----

    ### SRDH - State Resident DataHub

    ====

    ## NPR & NRC 

    ----

    <iframe src="http://censusup.gov.in/writedata/manuals/pretest/NPRMobileAppManualv1_6.pdf" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">

    </iframe>

    ====

    ## ECI and Aadhaar

    ----

    <iframe src="https://ia803001.us.archive.org/6/items/APECISRDHAlgorithms/AP%20ECI%20SRDH%20Algorithms.pdf" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">

    </iframe>

    ----

    <iframe src="https://www.thequint.com/news/india/elections-2019-seva-mitra-tdp-app-probe-voter-privacy-data-breach" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">

    </iframe>

    ====
    
    ## Cashless Economy & UPI

    ----
    
    ## UPI

    ----

    ### Current State of transactions

    ----

    - CC PoS is 2.4 lakh cr
    - Dbt PoS is 1.6 lakh cr
    - Total is 4 lakh Cr 

    ----

    ### What  if all go online?

    ----

    - 100% Online is 80 lakh  cr
    - 2% of Banking charges is 1.6 lakh cr
    - 49% FDI
    - Requires atleast 25 times the current infrastructure 

    ----

    ## NPCI

    ----

    <iframe src="https://www.npci.org.in/board-of-directors/" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">

    </iframe>

    ====

    ## PPP (Public Private Partnership)

    ----

    <iframe src="https://kstart.in/wp-content/uploads/2018/04/Imagining-Trillion-Dollar-Digital-India-IBM-Kalaari-Research-Report-April-2018.pdf" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">

    </iframe>

    ====

    ## Digital India 

    ----

    <iframe src="https://www.thequint.com/news/india/digital-india-services-create-problems-than-solutions-in-jharkhand" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">

    </iframe>

    ====

    ## Survillence 
    ****

    **Capitalism & Slavery**
    
    ====

    ## Survillence Capitalism

    ----

    ### Human experience as a commodity

    ----

    “Surveillance capitalism unilaterally claims human experience as free raw material for translation into behavioral data.”
    ― Shoshana Zuboff, The Age of Surveillance Capitalism 

    ====

    ## Survillence Slavery

    ----

    <iframe src="https://www.huffingtonpost.in/entry/swacch-bharat-tags-sanitation-workers-to-live-track-their-every-move_in_5e4c98a9c5b6b0f6bff11f9b" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">

    </iframe>

    
    ====

    ## Data Economy is the future ? 

    ----

    ## “it is no longer enough to automate information flows about us; the goal now is to automate us.” 
    
    ----

    1. What about the poor that has no choice, but survive ?
    2. What about my choice and my privacy ?
    3. Do companies that want to built privacy respecting products have a choice ?
    4. What is our future ?

    ----

    1. Solutions for the commons ?
    2. Solutions for the alternatives ?
    3. Public policy interventions (Personal Data Protection Bill - 2019)
    4. People movements and its role. 

    



    










