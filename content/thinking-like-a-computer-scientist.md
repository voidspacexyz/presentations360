---
Title: Thinking like a Computer Scientist
Date: 2010-12-03 10:20
Modified: 2017-05-08 19:30
Category: Python
Tags: computer,problems,problem-solving
Slug: computer-scientist
Authors: Ramaseshan
Summary: Introduction to Problem Solving
Template: revealjs
Theme: simple
Intro: name
Contact: contact
Transition: concave
Draft: yes
---

	### What is a computer ?
	
	----
	
	### What is a software ?

	====

	** What does any program(code) basically do ? **

	- Input <!-- .element: class="fragment" data-fragment-index="1" -->
	- Output <!-- .element: class="fragment" data-fragment-index="2" -->
	- Control Flow <!-- .element: class="fragment" data-fragment-index="3" -->
	- Repetation <!-- .element: class="fragment" data-fragment-index="4" -->
	- Data Manipulation <!-- .element: class="fragment" data-fragment-index="5" -->
	- Memory Management <!-- .element: class="fragment" data-fragment-index="6" -->

	====

	Lets Solve some problems ...!

	====

	** The Fizz Buzz Problem. **

	> Write a program that prints the numbers from 1 to 100. But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”. For numbers which are multiples of both three and five print “FizzBuzz”.

	====

	** Alternate Case Characters**

	> Without using upper() or lower() or any other related functions, can you print AbCdEf....

	====

	** Writing our own Random function **

	> Lets write our own random.h

	====

	** Before Coding ? **

	- Thinking of a solution before coding. <!-- .element: class="fragment" data-fragment-index="1" -->
	- Logic 	<!-- .element: class="fragment" data-fragment-index="2" -->
	- Write psuedo code	<!-- .element: class="fragment" data-fragment-index="3" -->
	- Execute the psuedo code	<!-- .element: class="fragment" data-fragment-index="4" -->
	- Validate all conditions and loops	<!-- .element: class="fragment" data-fragment-index="5" -->
	- Now write code in the computer. <!-- .element: class="fragment" data-fragment-index="6" -->

	====

	** Readibility / Comments **

	a = 4 # Declaring value of a to 4  <!-- .element: class="fragment" data-fragment-index="1" -->

	car_no_of_wheels = 4 # No of wheels of a car  <!-- .element: class="fragment" data-fragment-index="2" -->

	====

	** Experimental debugging**

	Programming is error-prone.
	
	====
	
	Old Monk VS The New Shiny Thing
	
	====
	
	Types of Computer Engineers
	
	- The random programmers (like me)
	- Research Engineers (Old Monks on Steroids)
	- Algorithmic programmers (people who flunked in math like me, dont fit in here)

	====
	
	![Wheels, We are too busy](theme/img/compsci/toobusy.jpg)
	
	====

	As a college student,  with programming and FOSS, what can I do ?

