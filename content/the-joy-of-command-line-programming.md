---
Title: The joy of command line programming
Date: 2020-06-28
Category: CLI,GNU/Linux
Tags: computer,cli
Slug: joy-of-cli
Authors: Ramaseshan
Summary: A getting started guide to the Terminal User Interface
Template: revealjs
Theme: simple
Intro: name
Contact: contact
Transition: concave
Draft: yes
---

~~~markdown
### GNU/Linux

====

What is a command ?

----

`ls` - List all files in a given directory

----

`pwd` - Present Working Directory

----

`cd path` - Change to the given directory

====

The atonomy of a command

----

command --flags options 

---
ls -lh .

----

`man ls` - The Manual Command

----

Lets look at a few more examples

Note:
    - ls -latrh ~
    - cd ~/something
    - cd /tmp/testing
    - mkdir /tmp/hello

====

Where is my file ?

----

find /path/ -name "something.*"  - In the given path find a file matching pattern something.{txt,odt,py etc}

----

find /path/ -name "something.*" -delete - In the given path find a file matching pattern something.{txt,odt,py etc} and ***delete it***

====

List all files where my function is called

----

grep salt-master content/* - List all files and lines where the word salt is there

----

grep -r salt-master . - ***Recursively scan all directories and files*** and list all files and strings container the work salt

----

grep -ir salt-master . - ***Case Insensitive and Recursively scan*** all directories and files and list all files and strings container the work salt

----

grep -irn salt-master . - ***List all files with line number *** of case insensitive and recursively scan all directories and files and list all files and strings container the work salt

====

### Pipes

The fundamental structure of Command Line Programming

----

cat ./content/intro-to-salt-stack.md | grep -in salt-master

----

====

### Greppability matters

====

### Text Files are awesome.

====

### Command line calculator

----

`bc`

====

### Generating files

----

echo "test1,hello,40" | tee -a hello.txt

----

``` for i in {0..100}; do; echo "test$i hello$RANDOM $RANDOM" | tee -a hello.txt; done ```

====

### AWK - The programming starts

----

cat hello.txt | awk '{print $1}'

----

#### Custom delimiter

cat hello.txt | awk -F"," '{print $1"--"$3}' 

----

#### Sum of the last column

----

cat hello.txt | awk -F"," '{print $3}' | paste -sd+ | bc

----

#### Column the data

cat hello.txt | column -ts ","

----

#### If column value greater than

cat hello.txt | awk -F"," '{if($3 > 1000){print "Greater than 1000"} else {print "Less than 1000"}}'

====

#### sed the editor

----

#### Replace , with space

cat hello.txt | sed "s/,/ /g"

----

Replace contents in the file

sed -i "s/,/ /g" hello.txt

====

### ~/.bashrc

----

Writing our own weather command

alias weather='function _weatherinfo(){ curl https://wttr.in/"$1"; }; _weatherinfo'

----

source ~/.bashrc 

----

weather Bangalore


====

### Further Explores

- vim (or emacs) for text editing 
- taskwarrior for task management 
- tmux
- bashrc hacks
- ZSH and Oh-my-ZSH
- Regular Expressions

====

![Wheels, We are too busy](theme/img/compsci/toobusy.jpg)


~~~

