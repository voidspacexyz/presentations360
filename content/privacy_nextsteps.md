---
Title: Privacy Self-Defense
Date: 2020-04-10 06:10
Modified: 2020-04-10 06:10
Category: Networks
Tags: mesh-network, community-control, data-commodification, self-hosting
Slug: privacy-nextsteps
Authors: VoidSpaceXYZ
Summary:  What are the next steps you can do to protect your privacy
Template: revealjs
Theme: simple
Intro: name
Contact: contact
Transition: concave
Draft: yes
---
    ## Individual Efforts

    ----

    ### Use Free and OpenSource Software(FOSS) only 

    ----

    ## GNU/Linux (Debian, Fedora, Prabola etc)

    ----

    # Laptops / Desktops

    ----

    ## Browser : Firefox

    ----

    * **[uBlock](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)**
    * **Privacy Badgr**
    * **Facebook/Google/Twitter container**
    * **Multi account container**

    ----

    ### Use alternative clients 

    * Youtube.com -> https://invidio.us/ 
    * Twitter/Facebook -> m.twitter/facebook.com (there is no desktop alternative)
    * Email (Gmail, outlook etc) -> Thunderbird
    * GApps (drive, office etc) -NextCloud 

    ----

    ## Password : BitWarden

    ----

    ## Desktop Apps 

    * **Zim Wiki** 
    * **LibreOffice**
    * **VLC** 

    ----

    # Mobile (Android)

    ----

    ## OS : Lineage OS

    ----

    * **Blockada v4**
    * **Slim Social(Twitter)**
    * **NewPipe**
    * **K-9 Mail**
    * **DAVx5**

    ====

    # Adopting the alternatives 

    ----

    * NextCloud
    * FireflyIII
    * Matomo analytics 
    * Mastadon (Social media)
    * PeerTube
    * OpenStreetMap

    ====

    ## Mapping all survillence in OpenStreetMap (CCTV etc)

    ====

    ## Signal 

    A confused suggesstion. 

    ====

    # Distribute
    # Decentralize
    # Democratize 

    ====

    ### Stay away from any form of digital data processing from the government (Aadhaar, NPR etc)

    ====

    ## PDP Bill 2019

    ----

    * Principles of necessity and propotionality

    ----

    <iframe src="https://sflc.in/key-changes-personal-data-protection-bill-2019-srikrishna-committee-draft" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">




