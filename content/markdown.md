---
Title: Free Software Free Society
Date: 2020-04-05 10:20
Modified: 2010-12-05 19:30
Category: FOSS
Tags: pelican, publishing
Slug: free-software
Authors: VoidSpaceXYZ
Summary: Intro to Free Software
Template: revealjs
Theme: simple
Intro: name
Contact: contact
Transition: concave
---

    ![GAFAM Octopus](theme/static/img/survillence/GAFAM-Octopus.png)
    
    ====

    ### Banking Concept of Education

    ![Banking Concept of Education](theme/img/freesoftware/banking_concept.png)

    Note: 
    - https://umsl.edu/~alexanderjm/The%20Banking%20Concept%20of%20Education.pdf
    - Author : Paulo Freire 

    ====

    ## What is a technology ?

    ----

    ## What is a Software ?

    ====

    # What is Free?

    ----

    ![Grinder Mixie](theme/img/freesoftware/grinder.jpg) <!-- .element: style="width: 26%;" class="fragment" data-fragment-index="1" -->
    ![Free Laptop](theme/img/freesoftware/laptop.jpg) <!-- .element style="width: 37%; height: 252px;" class="fragment" data-fragment-index="2"-->

    ![Fan](theme/img/freesoftware/fan.jpg) <!-- .element: style="width: 26%;" class="fragment" data-fragment-index="3" -->
    ![BaliAadu](theme/img/freesoftware/goat-Berari.preview.jpg)  <!-- .element style="width: 37%;" class="fragment" data-fragment-index="4"-->

    ----

    ![Free Coffee](theme/img/freesoftware/freeasincoffee.png) <!-- .element: style="width: 80%" -->

    ----

    ## FREE AS IN
    # (FREE)DOM

    ====

    # Free(dom) / Libre Software

    ----

    - Why does software freedom matter ?
      - Collective growth 
      - Public knowledge 
      - Localized development of features 

    ====

    # 4
    ## FREEDOMS

    ----

    ![Freedom 0](theme/img/freesoftware/freedom0.png) <!-- .element: style="width: 80%" -->

    ----

    ![Freedom1 1](theme/img/freesoftware/freedom1.png) <!-- .element: style="width: 80%" -->

    ----

    ![Freedom 2](theme/img/freesoftware/freedom2.png) <!-- .element: style="width: 80%" -->

    ----

    ![Freedom 3](theme/img/freesoftware/freedom3.png) <!-- .element: style="width: 80%" -->

    ====

    ![Richard Stallman](theme/img/freesoftware/richard_stallman.jpg) <!-- .element: style="width: 80%" -->

    ----

    ![Linux Torvalds](theme/img/freesoftware/LinusTorvalds.jpg) <!-- .element: style="width: 80%" -->

    ----

    # GNU/Linux

    ![GNU/Linux](theme/img/freesoftware/gnu_linux.png) <!-- .element: style="width: 80%" -->

    ====

    ## WOW  <!-- .element: class="fragment" data-fragment-index="1" -->
    ## Awesome right ??  <!-- .element: class="fragment" data-fragment-index="2" -->
    ## But wait !! Who uses this Free Software ??  <!-- .element: class="fragment" data-fragment-index="3" -->

    ----

    ![IBM](theme/img/freesoftware/ibm.jpg) <!-- .element: style="width: 35%; height: 208px;" -->
    ![ISS](theme/img/freesoftware/iss.jpg) <!-- .element: style="width: 35%;" -->
    ![GermanUniversity](theme/img/freesoftware/GermanUniversity.jpg) <!-- .element:style="width:35%"-->
    ![Nasa](theme/img/freesoftware/nassa.png) <!-- .element: style="width: 35%;"-->

    ====

    ## What about end users?

    ----

    ![Software Libre](theme/img/freesoftware/softwarelibre.png)

    ![VLC](theme/img/freesoftware/vlc.png) <!-- .element: style="width: 15%"-->
    ![Firefox](theme/img/freesoftware/firefox.png)  <!-- .element: style="width: 16%"-->

    ![Fedora](theme/img/freesoftware/FedoraLogo2.png)  <!-- .element: style="width: 20%"-->
    ![Debian](theme/img/freesoftware/debian.gif) <!-- .element: style="width: 20%"-->
   
    ====

    ## Free Software
    ### Vs
    ## Open Source

    ----

    ## GNU General Public License
    ### (GPLv3)

    ----

    ![Creative Commons](theme/img/freesoftware/cc.png)

    ----

    ## MIT
    ## BSD
    ## Apache

    ----

    ##  Ownership of knowledge matters !

    ----

    ##  Copyright
    ### Vs
    ## Copyleft


    ----

    ## Patents
   ![Patents](theme/img/freesoftware/patent.png)  <!-- .element: style="width: 80%;" -->

    ----

    ## Open Access
    ![Open Access](theme/img/freesoftware/openacess.png)  <!-- .element: style="width: 40%; float: left" -->
    ![Aaron_Swartz](theme/img/freesoftware/Aaron_Swartz.jpg)  <!-- .element: style="width: 50%; float: right" -->

    ====

    # Hackers 
    ### and 
    # Makers


    ====

    ## Action Items

    - Install GNU/Linux <!-- .element: class="fragment" data-fragment-index="1" -->
    - Use Free Software <!-- .element: class="fragment" data-fragment-index="2" -->
    - Join Community Mailing List <!-- .element: class="fragment" data-fragment-index="3" -->
    - Join IRC <!-- .element: class="fragment" data-fragment-index="4" -->
    - Write / Talk about it <!-- .element: class="fragment" data-fragment-index="5" -->

