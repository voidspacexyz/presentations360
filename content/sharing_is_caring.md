---
Title: Sharing is Caring
Date: 2020-04-05 10:20
Modified: 2010-12-05 19:30
Category: FOSS
Tags: pelican, publishing
Slug: sharing-is-caring
Authors: VoidSpaceXYZ
Summary: Intro to Free Software
Template: revealjs
Theme: simple
Intro: coopintro
Contact: coopcontact
Transition: concave
---

    ====
    
    ![Meme](theme/img/pon_meme_3.jpg) <!-- .element: style="width: 50%" -->
    
    ----
    
    ![Meme](theme/img/pon_meme_2.jpg) <!-- .element: style="width: 50%" -->
    
    ----
    
    ![Meme](theme/img/pon_meme_1.jpg) <!-- .element: style="width: 50%" -->
    
    ----
    
    ![Meme](theme/img/pon_meme_4.jpg) <!-- .element: style="width: 50%" -->
    
    
    ====


    ![Apples](theme/img/lot_of_apples.jpg) <!-- .element: style="width: 80%" -->
    
    ----

    ![Sharing](theme/img/sharing.webp) <!-- .element: style="width: 80%" -->
    
    ====
    
    But how has sharing changed today ?
    
    ----
    
    ![mz_laughed](theme/img/mz_laughed.jpeg) <!-- .element: style="width: 80%" -->
    
    ----
    
    ![Siri Alexa](theme/img/siri_alexa_laughed.jpg) <!-- .element: style="width: 80%" -->
    
    ====
    
    Sharing is Piracy
    
    
    ![Piracy is crime](theme/img/piracy-crime.jpg) <!-- .element: style="width: 80%" -->
    
    ----
    
    ![Sharing is not Priacy](theme/img/sharing_is_not_piracy.jpg) <!-- .element: style="width: 80%" -->
    
    ----
    
    Copying is not theft
    
    <iframe width="560" height="315" src="https://www.youtube.com/embed/IeTybKL1pM4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    
    ====
    
    ## FREE SOFTWARE
    
    ----
    
    ![Free Coffee](theme/img/freesoftware/freeasincoffee.png) <!-- .element: style="width: 80%" -->

    ----

    ## FREE AS IN
    # (FREE)DOM

    ====

    # Free(dom) / Libre Software

    ----

    - Why does software freedom matter ?
      - Collective growth 
      - Public knowledge 
      - Localized development of features 

    ====

    # 4
    ## FREEDOMS

    ----

    ![Freedom 0](theme/img/freesoftware/freedom0.png) <!-- .element: style="width: 80%" -->

    ----

    ![Freedom1 1](theme/img/freesoftware/freedom1.png) <!-- .element: style="width: 80%" -->

    ----

    ![Freedom 2](theme/img/freesoftware/freedom2.png) <!-- .element: style="width: 80%" -->

    ----

    ![Freedom 3](theme/img/freesoftware/freedom3.png) <!-- .element: style="width: 80%" -->

    ====

    ![Richard Stallman](theme/img/freesoftware/richard_stallman.jpg) <!-- .element: style="width: 80%" -->

    ----

    ![Linux Torvalds](theme/img/freesoftware/LinusTorvalds.jpg) <!-- .element: style="width: 80%" -->

    ----

    ### GNU/Linux

    ![GNU/Linux](theme/img/freesoftware/gnu_linux.png) <!-- .element: style="width: 80%" -->
    
    ----
    
    # HACKERS  
    #### and
    # MAKERS


    ====

    ![GAFAM](theme/img/gafam.jpg) <!-- .element: style="width: 80%" -->


    ----
              
    #### DEPRIVACY("The Assault on Privacy" by Felicia Lamport)
        Although we feel unknown, ignored  
        As unrecorded blanks,  
        Take heart! Our vital selves are stored  
        In giant data banks,  
        Our childhoods and maturities,  
        Efficiently compiled,  
        Our Stocks and insecurities,  
        All permanently filed,  
        Our tastes and our proclivities,  
        In gross and in particular,  
        Our incomes, our activities  
        Both extra-and curricular.  
        And such will be our happy state  
        Until the day we die  
        When we’ll be snatched up by the great  
        Computer in the Sky  
    

    ====

    ## Action Items

    - Install GNU/Linux <!-- .element: class="fragment" data-fragment-index="1" -->
    - Use Free Software <!-- .element: class="fragment" data-fragment-index="2" -->
    - Join Community Mailing List <!-- .element: class="fragment" data-fragment-index="3" -->
    - Join Free Software Communities near you <!-- .element: class="fragment" data-fragment-index="4" -->
    - Write / Talk about it <!-- .element: class="fragment" data-fragment-index="5" -->

